FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/*.jar /app.jar
ENTRYPOINT java $JAVA_OPTS -jar "/app.jar"