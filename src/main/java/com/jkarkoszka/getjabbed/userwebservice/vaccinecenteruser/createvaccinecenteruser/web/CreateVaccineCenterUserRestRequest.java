package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.web;

import lombok.Builder;
import lombok.Value;

/**
 * Create vaccince center user rest request payload.
 */
@Value
@Builder
public class CreateVaccineCenterUserRestRequest {

  String name;
  String username;
  String password;
  String registerToken;
}
