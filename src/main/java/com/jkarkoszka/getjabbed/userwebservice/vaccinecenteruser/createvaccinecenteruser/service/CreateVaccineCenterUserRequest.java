package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.service;

import lombok.Value;

/**
 * Create vaccine center user request payload.
 */
@Value
public class CreateVaccineCenterUserRequest {

  String name;
  String username;
  String password;
  String registerToken;
}
