package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.service;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.security.Role;
import com.jkarkoszka.getjabbed.userwebservice.common.user.db.UserDb;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class VaccineCenterUserDbCreator {

  private final PasswordEncoder passwordEncoder;

  UserDb create(CreateVaccineCenterUserRequest createVaccineCenterUserRequest,
                VaccineCenterRest vaccineCenterRest) {
    return UserDb.builder()
        .id(new ObjectId())
        .name(createVaccineCenterUserRequest.getName())
        .username(createVaccineCenterUserRequest.getUsername())
        .password(passwordEncoder.encode(createVaccineCenterUserRequest.getPassword()))
        .role(Role.ROLE_VACCINE_CENTER)
        .vaccineCenterId(vaccineCenterRest.getId())
        .createdAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
