package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.web;

import com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.service.CreateVaccineCenterUserRequest;
import org.mapstruct.Mapper;

/**
 * Mapper CreateVaccineCenterUserRequest/CreateVaccineCenterUserRestRequest.
 */
@Mapper
public interface CreateVaccineCenterUserRequestMapper {

  CreateVaccineCenterUserRequest map(CreateVaccineCenterUserRestRequest restRequest);

}
