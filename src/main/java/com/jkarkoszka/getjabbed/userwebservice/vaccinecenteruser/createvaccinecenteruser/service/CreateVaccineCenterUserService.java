package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.service;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterWebserviceClient;
import com.jkarkoszka.getjabbed.security.autoconfigure.SecurityProperties;
import com.jkarkoszka.getjabbed.userwebservice.common.user.db.UserDbRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to create vaccine center user.
 */
@Component
@AllArgsConstructor
public class CreateVaccineCenterUserService {

  private final VaccineCenterWebserviceClient vaccineCenterWebserviceClient;
  private final UserDbRepository userDbRepository;
  private final SecurityProperties securityProperties;
  private final VaccineCenterUserDbCreator vaccineCenterUserDbCreator;

  /**
   * Method to create vaccine center user.
   *
   * @param createVaccineCenterUserRequest payload
   * @return empty mono
   */
  public Mono<Void> create(CreateVaccineCenterUserRequest createVaccineCenterUserRequest) {
    return vaccineCenterWebserviceClient.findByRegisterToken(
            securityProperties.getBackendServiceToken(),
            createVaccineCenterUserRequest.getRegisterToken())
        .map(vaccineCenterRest -> vaccineCenterUserDbCreator.create(createVaccineCenterUserRequest,
            vaccineCenterRest))
        .flatMap(userDbRepository::save)
        .then();
  }
}
