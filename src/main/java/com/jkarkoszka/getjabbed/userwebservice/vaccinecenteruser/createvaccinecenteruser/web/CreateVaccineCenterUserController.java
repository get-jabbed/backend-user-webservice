package com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.web;

import com.jkarkoszka.getjabbed.userwebservice.vaccinecenteruser.createvaccinecenteruser.service.CreateVaccineCenterUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller with methods to create vaccine center user.
 */
@RestController
@AllArgsConstructor
public class CreateVaccineCenterUserController {

  private final CreateVaccineCenterUserService createVaccineCenterUserService;
  private final CreateVaccineCenterUserRequestMapper createVaccineCenterUserRequestMapper;

  @PostMapping("/vaccine-center-user")
  public Mono<Void> register(@RequestBody CreateVaccineCenterUserRestRequest restRequest) {
    return Mono.fromSupplier(() -> createVaccineCenterUserRequestMapper.map(restRequest))
        .flatMap(createVaccineCenterUserService::create);
  }
}
