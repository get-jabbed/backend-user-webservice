package com.jkarkoszka.getjabbed.userwebservice.common.gettoken.web;

import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import com.jkarkoszka.getjabbed.security.service.JwtTokenGeneratorService;
import com.jkarkoszka.getjabbed.userwebservice.common.user.security.GetJabbedAuthenticationManager;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller with method to get JWT token.
 */
@RestController
@AllArgsConstructor
public class GetTokenController {

  private final JwtTokenGeneratorService jwtTokenGeneratorService;
  private final GetJabbedAuthenticationManager getJabbedAuthenticationManager;

  /**
   * Endpoint to get JWT token.
   *
   * @param restRequestMono rest request payload
   * @return response with JWT token
   */
  @PostMapping("/token")
  public Mono<GetTokenRestResponse> getToken(
      @Valid @RequestBody Mono<GetTokenRestRequest> restRequestMono) {
    return restRequestMono
        .flatMap(restRequest -> getJabbedAuthenticationManager
            .authenticate(GetJabbedAuthenticationTokenCreator.create(restRequest))
            .filter(authentication -> authentication instanceof GetJabbedAuthenticationToken)
            .map(authentication -> (GetJabbedAuthenticationToken) authentication)
            .map(jwtTokenGeneratorService::createToken)
        )
        .map(GetTokenRestResponse::new);
  }
}
