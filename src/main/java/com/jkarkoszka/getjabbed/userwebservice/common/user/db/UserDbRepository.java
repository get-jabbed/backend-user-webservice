package com.jkarkoszka.getjabbed.userwebservice.common.user.db;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

/**
 * User db repository.
 */
public interface UserDbRepository extends ReactiveMongoRepository<UserDb, ObjectId> {

  Mono<UserDb> findByUsername(String username);
}
