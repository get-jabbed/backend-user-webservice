package com.jkarkoszka.getjabbed.userwebservice.common.gettoken.web;

import com.jkarkoszka.getjabbed.security.Role;
import lombok.Builder;
import lombok.Value;

/**
 * Get token rest request payload.
 */
@Value
@Builder
public class GetTokenRestRequest {

  String username;
  String password;
  Role role;
}
