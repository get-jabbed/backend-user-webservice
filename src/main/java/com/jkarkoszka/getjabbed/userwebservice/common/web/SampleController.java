package com.jkarkoszka.getjabbed.userwebservice.common.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Sample controller.
 */
@RestController
public class SampleController {

  @Value("${getjabbed.trigger-cd}")
  private String triggerCd;

  @GetMapping("/test")
  public Mono<String> testEndpoint() {
    return Mono.just("Test - " + triggerCd);
  }
}
