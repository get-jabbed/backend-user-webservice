package com.jkarkoszka.getjabbed.userwebservice.common.user.security;

import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * Custom ReactiveAuthenticationManager for GetJabbed user.
 */
public class GetJabbedAuthenticationManager implements ReactiveAuthenticationManager {

  private PasswordEncoder passwordEncoder =
      PasswordEncoderFactories.createDelegatingPasswordEncoder();

  private Scheduler scheduler = Schedulers.boundedElastic();

  private ReactiveUserDetailsService patientUserDetailsService;

  public GetJabbedAuthenticationManager(ReactiveUserDetailsService patientUserDetailsService) {
    this.patientUserDetailsService = patientUserDetailsService;
  }

  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    String username = authentication.getName();
    String presentedPassword = (String) authentication.getCredentials();
    // @formatter:off
    return retrieveUser(username)
        .publishOn(this.scheduler)
        .filter((userDetails) -> this.passwordEncoder.matches(presentedPassword,
            userDetails.getPassword()))
        .switchIfEmpty(
            Mono.defer(() -> Mono.error(new BadCredentialsException("Invalid Credentials"))))
        .map(this::createGetJabbedAuthenticationToken);
    // @formatter:on
  }

  private GetJabbedAuthenticationToken createGetJabbedAuthenticationToken(
      GetJabbedUser getJabbedUser) {
    return new GetJabbedAuthenticationToken(getJabbedUser.getUsername(),
        getJabbedUser.getPassword(), getJabbedUser.getAuthorities(),
        getJabbedUser.getVaccineCenterId());
  }

  /**
   * The {@link PasswordEncoder} that is used for validating the password. The default
   * is {@link PasswordEncoderFactories#createDelegatingPasswordEncoder()}
   *
   * @param passwordEncoder the {@link PasswordEncoder} to use. Cannot be null
   */
  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
    Assert.notNull(passwordEncoder, "passwordEncoder cannot be null");
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Sets the {@link Scheduler} used by the
   * {@link UserDetailsRepositoryReactiveAuthenticationManager}. The default is
   * {@code Schedulers.newParallel(String)} because modern password encoding is a CPU
   * intensive task that is non blocking. This means validation is bounded by the number
   * of CPUs. Some applications may want to customize the {@link Scheduler}. For
   * example, if users are stuck using the insecure
   * {@link org.springframework.security.crypto.password.NoOpPasswordEncoder} they might
   * want to leverage {@code Schedulers.immediate()}.
   *
   * @param scheduler the {@link Scheduler} to use. Cannot be null.
   * @since 5.0.6
   */
  public void setScheduler(Scheduler scheduler) {
    Assert.notNull(scheduler, "scheduler cannot be null");
    this.scheduler = scheduler;
  }

  /**
   * Allows subclasses to retrieve the <code>UserDetails</code> from an
   * implementation-specific location.
   *
   * @param username The username to retrieve
   * @return the user information. If authentication fails, a Mono error is returned.
   */
  private Mono<GetJabbedUser> retrieveUser(String username) {
    return this.patientUserDetailsService.findByUsername(username)
        .filter(userDetails -> userDetails instanceof GetJabbedUser)
        .map(userDetails -> (GetJabbedUser) userDetails);
  }
}
