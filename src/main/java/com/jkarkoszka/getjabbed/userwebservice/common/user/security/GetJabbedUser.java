package com.jkarkoszka.getjabbed.userwebservice.common.user.security;

import java.util.Collection;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * Custom user implementation for GetJabbed.
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class GetJabbedUser extends User {

  private final String vaccineCenterId;

  @Builder(builderMethodName = "getJabbedBuilder")
  public GetJabbedUser(String username, String password,
                       Collection<? extends GrantedAuthority> authorities,
                       String vaccineCenterId) {
    super(username, password, authorities);
    this.vaccineCenterId = vaccineCenterId;
  }
}
