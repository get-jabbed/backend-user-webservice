package com.jkarkoszka.getjabbed.userwebservice.common.user.security.config;

import com.jkarkoszka.getjabbed.userwebservice.common.user.db.UserDbRepository;
import com.jkarkoszka.getjabbed.userwebservice.common.user.security.GetJabbedAuthenticationManager;
import com.jkarkoszka.getjabbed.userwebservice.common.user.security.GetJabbedUser;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * GetJabbedUser specific security configuration.
 */
@Configuration
@AllArgsConstructor
public class GetJabbedUserSecurityConfiguration {

  private final UserDbRepository userDbRepository;

  @Bean
  GetJabbedAuthenticationManager reactiveAuthenticationManager(
      ReactiveUserDetailsService userDetailsService,
      PasswordEncoder passwordEncoder) {
    var authenticationManager =
        new GetJabbedAuthenticationManager(userDetailsService);
    authenticationManager.setPasswordEncoder(passwordEncoder);
    return authenticationManager;
  }

  @Bean
  ReactiveUserDetailsService userDetailsService() {
    return (username) -> userDbRepository.findByUsername(username)
        .map(userDb -> GetJabbedUser.getJabbedBuilder()
            .username(userDb.getUsername())
            .password(userDb.getPassword())
            .authorities(List.of(new SimpleGrantedAuthority(userDb.getRole().name())))
            .vaccineCenterId(userDb.getVaccineCenterId())
            .build()
        );
  }
}
