package com.jkarkoszka.getjabbed.userwebservice.common.gettoken.web;

import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import java.util.List;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

class GetJabbedAuthenticationTokenCreator {

  static GetJabbedAuthenticationToken create(GetTokenRestRequest restRequest) {
    return new GetJabbedAuthenticationToken(restRequest.getUsername(),
        restRequest.getPassword(),
        List.of(new SimpleGrantedAuthority(restRequest.getRole().name())), null);
  }
}
