package com.jkarkoszka.getjabbed.userwebservice.common.gettoken.web;

import lombok.Value;

/**
 * Get token rest response payload.
 */
@Value
public class GetTokenRestResponse {

  String token;
}
