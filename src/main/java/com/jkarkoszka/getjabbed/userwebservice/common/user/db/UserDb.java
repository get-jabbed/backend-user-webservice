package com.jkarkoszka.getjabbed.userwebservice.common.user.db;

import com.jkarkoszka.getjabbed.security.Role;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * User db model.
 */
@Document(collection = "users")
@Data
@Builder
public class UserDb {

  @Id
  private ObjectId id;

  private Role role;

  private String name;

  private String username;

  private String password;

  private LocalDateTime createdAt;

  private LocalDateTime updatedAt;

  private String vaccineCenterId;

}
