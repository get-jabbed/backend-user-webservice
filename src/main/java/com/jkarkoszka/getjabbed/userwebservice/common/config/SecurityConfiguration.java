package com.jkarkoszka.getjabbed.userwebservice.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * Configuration of the security.
 */
@Configuration
public class SecurityConfiguration {

  @Bean
  SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {
    return http
        .authorizeExchange(this::prepareAuthorizeExchange)
        .build();
  }

  private ServerHttpSecurity.AuthorizeExchangeSpec prepareAuthorizeExchange(
      ServerHttpSecurity.AuthorizeExchangeSpec it) {
    return it.anyExchange().permitAll();
  }
}
