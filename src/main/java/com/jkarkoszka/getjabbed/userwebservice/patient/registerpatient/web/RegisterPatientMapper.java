package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.web;

import com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.service.RegisterPatientRequest;
import org.mapstruct.Mapper;

/**
 * Mapper RegisterPatientRequest/RegisterPatientRestRequest.
 */
@Mapper
public interface RegisterPatientMapper {

  RegisterPatientRequest map(RegisterPatientRestRequest restRequest);

}
