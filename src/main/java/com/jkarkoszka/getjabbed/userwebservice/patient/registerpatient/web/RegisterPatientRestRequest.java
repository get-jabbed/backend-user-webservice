package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.web;

import lombok.Builder;
import lombok.Value;

/**
 * Register patient rest request payload.
 */
@Value
@Builder
public class RegisterPatientRestRequest {

  String name;
  String username;
  String password;
}
