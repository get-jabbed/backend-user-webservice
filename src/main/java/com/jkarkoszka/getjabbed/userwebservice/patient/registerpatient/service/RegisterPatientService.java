package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.service;

import com.jkarkoszka.getjabbed.userwebservice.common.user.db.UserDbRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to register patients.
 */
@Component
@AllArgsConstructor
public class RegisterPatientService {

  private final UserDbRepository userDbRepository;
  private final PatientDbCreator patientDbCreator;

  /**
   * Method to register patient.
   *
   * @param registerPatientRequest register patienr request payload
   * @return mono empty
   */
  public Mono<Void> register(RegisterPatientRequest registerPatientRequest) {
    return Mono.fromSupplier(() -> patientDbCreator.create(registerPatientRequest))
        .flatMap(userDbRepository::save)
        .then();
  }
}
