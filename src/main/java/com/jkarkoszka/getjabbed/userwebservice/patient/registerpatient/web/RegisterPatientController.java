package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.web;

import com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.service.RegisterPatientService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller with methods to register patient.
 */
@RestController
@AllArgsConstructor
public class RegisterPatientController {

  private final RegisterPatientMapper registerPatientMapper;
  private final RegisterPatientService registerPatientService;

  /**
   * Endpoint to register patient.
   *
   * @param restRequest rest request payload
   * @return empty mono
   */
  @PostMapping("/patient")
  public Mono<Void> register(@RequestBody RegisterPatientRestRequest restRequest) {
    return Mono.fromSupplier(() -> registerPatientMapper.map(restRequest))
        .flatMap(registerPatientService::register);
  }
}
