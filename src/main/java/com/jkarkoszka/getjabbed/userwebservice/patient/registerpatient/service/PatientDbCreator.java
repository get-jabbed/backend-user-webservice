package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.service;

import com.jkarkoszka.getjabbed.security.Role;
import com.jkarkoszka.getjabbed.userwebservice.common.user.db.UserDb;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class PatientDbCreator {

  private final PasswordEncoder passwordEncoder;

  UserDb create(RegisterPatientRequest registerPatientRequest) {
    return UserDb.builder()
        .id(new ObjectId())
        .name(registerPatientRequest.getName())
        .username(registerPatientRequest.getUsername())
        .password(passwordEncoder.encode(registerPatientRequest.getPassword()))
        .role(Role.ROLE_PATIENT)
        .createdAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
