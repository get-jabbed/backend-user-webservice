package com.jkarkoszka.getjabbed.userwebservice.patient.registerpatient.service;

import lombok.Value;

/**
 * Register patient request payload.
 */
@Value
public class RegisterPatientRequest {

  String name;
  String username;
  String password;
}
